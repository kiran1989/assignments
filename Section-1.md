## Section 1 Terraform
### How would you manage state in Terraform for a team working on the same infrastructure? What best practices would you implement? Describe the steps you would take to ensure a secure and scalable deployment.?
If  more than one member or teams is working on terraform you need to ensure the below points.
* Use statefile in remote store instead of local (Use Storage Account, S3, etc.).
* As multiple users are wokring on different resources or modules you have to manage it as GitOps process.
* Enable versioning of statefile in storage account (to recover in case)
* If the infrastructure has multiple environments use `terraform workspace` command effectively.
* Effectively plan and design the terraform modules (based on resource, project or teams)
* If needed use sensitive variables for security.
* Bake the plan file out and apply the changes based on the plan file (this will avoid the wrong configuration deployments.)
* If you have so many terraform resources use `-parallelism=30` to improve perfomance.

1. Create azure storage account and container to store the statefile (make sure only relavent users have access to the storage account).
```
az storage account create -n tfstatestorage -g MyResourceGroup -l westeurope --sku Standard_LRS --encryption-services blob
az storage container create -n tfstate --account-name tfstatestorage
```
configure the statefile to use storage account using `backend` or pipeline task `TerraformTaskV4@4`.
```
terraform {
  # Backend reference that stores the state file
  backend "azurerm" {
    storage_account_name = "tfstatestorage"
    container_name       = "tfstate"
    key                  = "webapp.tfstate"
    resource_group_name  = "MyResourceGroup"
    subscription_id      = "xxx-xxx-xxx"
  }
}

```
2. Create a azure repo eg name: `infrastructure` (you can use any version control system). Setup protected branch as `main`. Setup PR builds and main builds.
* PR builds only validate plan and apply will be a dry run
* Once Pr is merged to main you can take the plan out and apply based on that plan.

3. Use terrafrom workspace command and design the variables accordingly. eg: This will only apply to acc
```
terraform workspace select acc
count           = terraform.workspace == "acc" ? 1 : 0
environment     = terraform.workspace
```
4. Store sensitive variables in azure devops pipleline library or keyvault and pass to the terraform via pipeline.
```
terraform plan -var "DD_SECRET=${DD_SECRET_VAL}"
```
5. Bake the plan and then apply.
```
terraform plan -out "${BUILDNUMBER}-{ENV}-tfplan"
terraform apply -auto-approve "${BUILDNUMBER}-{ENV}-tfplan"
```