
# Section 3: Azure DevOps
### How do you implement infrastructure as code (IaC) within Azure DevOps, and what are the benefits of this approach? Make sure to  include security checks and compliance validation.

1. In Azure devops i will use GitOps to implement the Iac and that should have the below stages.
![img](11.PNG "img")

2. It has the following benifits.
* Scalablity (As Iac belongs to repo n number of team can create there feature changes and validate via PR builds.)
* Secure (As we are establising the connections via service connections and using sensitive variables the IaC is secure.) 
* Versioning (As we are using the git we can version the builds depends on master merges.)
* Rollback (Its easy to roll back to previous state as we are keeping Versioning)
* Flexibility (Easy to integrate multiple stages or jobs if necessary)
* We can benefits features of GitOps.

3. Security checks and compliance validation.
We can benefit unit testing in Iac to make sure our Iac is compliant. like terratest, terragunt or terraform test
eg: GDPR compliance validation using terrafom test.
```
resource "azurerm_resource_group" "rg" {
  name     = "databricks-rg"
  location = "westeurope"
}

run "gdpr_compliance" {
  command = plan
  assert {
    condition     = azurerm_resource_group.rg.location == "westeurope"
    error_message = "Your rg is not compliant with GDPR"
  }
}
```
![img](12.PNG "img")