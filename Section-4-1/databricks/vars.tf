variable "location" {
  type = string
}

variable "tenant_id" {
  type    = string
  default = ""
}