resource "azurerm_resource_group" "rg" {
  name     = "databricks-rg"
  location = var.location
}

resource "azurerm_databricks_workspace" "databricks" {
  name                = "databricks-new"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "premium"

  managed_services_cmk_key_vault_key_id = azurerm_key_vault_key.cmk_key.id
  managed_disk_cmk_key_vault_key_id     = azurerm_key_vault_key.disk_cmk_key.id
  managed_resource_group_name           = "mgd-databricks-rg"
  customer_managed_key_enabled          = true
  public_network_access_enabled         = false

  custom_parameters {
    no_public_ip                                         = true
    virtual_network_id                                   = azurerm_virtual_network.vnet.id
    public_subnet_name                                   = azurerm_subnet.pubsubnet.id
    private_subnet_name                                  = azurerm_subnet.pvtsubnet.id
    public_subnet_network_security_group_association_id  = azurerm_network_security_group.pubnsg.id
    private_subnet_network_security_group_association_id = azurerm_network_security_group.pvtnsg.id
  }
}
