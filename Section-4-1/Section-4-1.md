## Section-4-1
### Write a Terraform script that provisions an Azure Databricks Workspace with security taking into account
Please review the terraform script written in modular way. I have included `terraform test` as well for your review.

1. Security Considerations.
* Created customer managed keys in KV to encrypt notebooks and artifacts.
* Created customer managed keys to encrypt workspace managed disks.
* Public access is disabled only can access through internal network (Users required a vpn connection).
* Network security groups to handle inbound and outbound traffic.