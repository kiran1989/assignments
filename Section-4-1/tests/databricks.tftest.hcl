run "gdpr_compliance" {
  command = plan
  assert {
    condition     =  module.databricks.rg_location == "westeurope"
    error_message = "Your rg is not compliant with GDPR"
  }
}
