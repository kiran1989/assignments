variable "location" {
  type    = string
  default = "West Europe"
}

variable "tenant_id" {
  type    = string
  default = ""
}