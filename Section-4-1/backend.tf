terraform {
  backend "azurerm" {
    storage_account_name = "tfstatecd"
    container_name       = "databricks"
    key                  = "terraform.tfstate"
    resource_group_name  = "sa-mgt-rg"
    subscription_id      = ""
  }
}
